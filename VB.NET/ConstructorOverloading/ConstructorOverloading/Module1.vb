﻿REM Q6. Write a program to calculate area of circle, square and triangle using
REM     constructor overloading.

Module Module1

    Class Area
        Dim pi As Double = 3.14
        Dim area As Double

        Sub New()
            'Area of circle'
            Console.WriteLine("Enter the Radius: ")
            Dim r As Double
            r = Console.ReadLine
            area = pi * r * r
        End Sub

        Sub New(ByVal side As Double)
            'Area of Square'
            area = side * side
        End Sub

        Sub New(ByVal h As Double, ByVal b As Double)
            'Area of Triangle'
            area = h * b / 2
        End Sub

        Sub display()
            Console.WriteLine(area)
        End Sub

    End Class


    Sub Main()
        Dim o As Area = New Area()
        Console.WriteLine("Area Of Circle : ")
        o.display()

        Dim r, h, b As Double
        Console.WriteLine("Enter the side : ")
        r = Console.ReadLine

        Dim o1 = New Area(r)
        Console.WriteLine("Area Of Square : ")
        o1.display()

        Console.WriteLine("Enter the Height : ")
        h = Console.ReadLine
        Console.WriteLine("Enter the Base : ")
        b = Console.ReadLine

        Dim o2 As Area = New Area(h, b)
        Console.Write("Area of Triangle : ")
        o2.display()

        Console.WriteLine("program completed press a key : ")
        Console.ReadKey()
    End Sub

End Module
