﻿REM Q1. Write a program in vb.net which copy the contents of a text file into another.

Module Module1

    Sub Main()
        Dim FileToCopy As String
        Dim NewCopy As String

        Console.Write("Enter File path to copy: ")
        FileToCopy = Console.ReadLine()

        Console.WriteLine("Enter File Copy To Path: ")
        NewCopy = Console.ReadLine()

        If System.IO.File.Exists(FileToCopy) = True Then
            System.IO.File.Copy(FileToCopy, NewCopy)
            Console.WriteLine("File is copied!")
        Else
            Console.WriteLine("File do not exist!")
        End If

        Console.WriteLine("Program Completed, Press any key to exit: ")
        Console.ReadKey()

    End Sub

End Module
