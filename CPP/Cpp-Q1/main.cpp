/**
 * Author: Aniket Satarkar <aniketsatarkar9@gmail.com>
 */

/**
 * Q1. Solve the following pattern,
 * A
 * A B
 * A B C
 * A B C D
 */

#include <iostream>

using namespace std;

#define START_CHAR 'A'  // character to start the sequence with
#define LINES 5         // number of lines to print

int main()
{
    for(int i=1; i<LINES; i++)
    {
        for(int j=START_CHAR; j < (START_CHAR+i); j++)
            cout <<  (char)j << " ";

        cout << endl;
    }

    // pause termination until user input
    cout << "\npress any key to exit: ";
    cin.get();

    return 0;
}
// end of main
