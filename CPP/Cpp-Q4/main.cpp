#include <iostream>
using namespace std;

/**
 * Author: Aniket Satarkar <aniketsatarkar9@gmail.com>
 *
 * Q4. Write a program to find maximum of three numbers using INLINE function.
 */


// inline function to find out greatest number among three,
// or if they are the same.
inline int largest_in_three(int a, int b, int c, bool &are_same)
{
    if(a > b && a > c)
        return a;
    else if(b > a && b > c)
        return b;
    else if(c > a && c > b)
        return c;
    else if(a == b && b == c && c == a)
    {
        are_same = true;
        return 0;
    }
}

int main()
{
    // get three integers from user
    int num_arr[3];
    for(int i=0; i<3; i++)
    {
        cout << "Enter number - " << i+1 << " : ";
        cin >> num_arr[i];
    }

    // inline function call
    bool are_same = false;
    int res = largest_in_three(num_arr[0], num_arr[1], num_arr[2], are_same);

    // check if the numbers are the same
    if(are_same)
        cout << "All numbers are the same!";
    else
        cout << res << " is the largest number!";

    // hold immediate termination of the console.
    cout << "\nPress any key to exit : ";
    cin.get();

    return 0;

}// end of main.
