+-----+
| C++ |
+-----+

Q1. Solve the following pattern,
A 
A B
A B C
A B C D

Q2. Write a program to overload <, > and == operator.

Q3. Write a program to transfer the contents of one text file to another 
    text file
	
Q4. Write a program to find maximum of three numbers using INLINE function.

Q5. Write a program to illustrate try-catch with division by zero exception.
