#include <iostream>
#include <fstream>

using namespace std;

/**
 * Author: Aniket Satarkar <aniketsatarkar9@gmail.com>
 *
 * Q3. Write a program to transfer the contents of one text file to another
 *     text file
 */

int main()
{
    // variables to hold file names.
    char file_path[100] = "";
    char copy_path[100] = "";
    string ch = "";

    // file input/output stream objects.
    ifstream ifs;
    ofstream ofs;

    input_file_path: // jump here if failed to open input file.

    cout << "\nEnter input file path : ";
    cin >> file_path;

    ifs.open(file_path);
    if(!ifs)
    {
        cout << "Filed to open " << file_path << " File.";
        goto input_file_path;
    }

    cout << "Enter copy file path : ";
    cin >> copy_path;
    ofs.open(copy_path);

    // check if output file stream is not null
    if(ofs)
    {
        // copy line by line to another file
        while(!ifs.eof())
        {
            getline(ifs, ch);
            cout << ch;
            ofs << ch;
        }
    }

    // close files.
    ifs.close();
    ofs.close();

    cout << file_path << " copied to " << copy_path << endl;

    cout << "Press any key to exit : ";
    cin.get();

    return 0;

}// end of main.
