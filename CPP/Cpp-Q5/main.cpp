#include <iostream>
using namespace std;

/**
 * Author: Aniket Satarkar <aniketsatarkar9@gmail.com>
 *
 * Q5. Write a program to illustrate try-catch with division by zero exception.
 */

int main()
{
    int numerator   = 0;
    int denominator = 0;

    cout << "enter numerator : ";
    cin >> numerator;

    cout << "\n";

    cout << "Enter denominator : ";
    cin >> denominator;

    try
    {
        // throw exception if denominator is zero
        if(denominator == 0)
            throw "division by zero exception occurred";

        cout << numerator << "/" << denominator
             << "=" << numerator/denominator
             << endl;
    }
    catch(const char *msg)
    {
        cout << "Exception : " << msg << endl;
    }

    // hold immediate termination of console
    cout << "\nPress any key to exit : ";
    cin.get();

    return 0;

}// end of main.
