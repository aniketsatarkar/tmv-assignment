/**
 * Author: Aniket Satarkar <aniketsatarkar9@gmail.com>
 */

/**
 * Q2. Write a program to overload <, > and == operator.
 */

#include <iostream>

using namespace std;

/**
 * Size class hold a width and height property,
 * object.
 */
class Size
{
public:

    // default constructor, with initialized properties.
    Size()
    {
        this->width = 0.0;
        this->height = 0.0;
    }

    // overloaded constructor
    Size(float _width, float _height)
    {
        this->width = _width;
        this->height = _height;
    }

    // setter function for width property
    void set_width(float _width)
    {
        this->width = _width;
    }

    // getter function for height property
    void set_height(float _height)
    {
        this->height = _height;
    }

    // a helper function to print Size object to console
    void print_size()
    {
        cout << this->width << "x" << this->height << endl;
    }

    // overloading 'equals to' operator for Size object.
    bool operator==(const Size &obj)
    {
        return (this->width == obj.width && this->height == obj.height);
    }

    // overloading 'less than' operator for Size object.
    bool operator<(const Size &obj)
    {
        float this_size = this->width * this->height;
        float obj_size = obj.width * obj.height;

        return (this_size < obj_size);
    }

    // overloading 'greater than' operator for Size objects.
    bool operator>(const Size &obj)
    {
        float this_size = this->width * this->height;
        float obj_size = obj.width * obj.height;

        return (this_size > obj_size);
    }

private:
    float width;    // width value
    float height;   // height value
};
// end of Size


int main()
{
    // ----------------------------------------------
    // comparing a large and a small sizes.
    Size large = Size(100, 110);
    Size small = Size(10, 11);

    cout << "large size : ";
    large.print_size();

    cout << "small size : ";
    small.print_size();

    cout << "Comparison : sizes are "
         << ((large == small)? "same" : "different")
         << endl;

    if(large > small)
        cout << "large size is really larger!" << "\n\n";

    // ----------------------------------------------
    // keeping both sizes same and comparing.
    small.set_width(100);
    small.set_height(110);

    cout << "large size : ";
    large.print_size();

    cout << "small size : ";
    small.print_size();

    cout << "Comparison : sizes are "
         << ((large == small)? "same" : "different")
         << "\n" <<endl;

    // ----------------------------------------------
    // increasing 'small' size larger than 'large' size
    // and comparing
    small.set_width(200);
    small.set_height(220);

    cout << "large size : ";
    large.print_size();

    cout << "small size : ";
    small.print_size();

    if(large < small)
        cout << "large size isn't really larger!" << "\n\n";


    // hold terminal termination until user input.
    cout << "\npress any key to exit: ";
    cin.get();

    return 0;
}
// end of main
